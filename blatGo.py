from os import listdir, system, remove
from os.path import isfile, join, exists

class blatGo:
	# ==== CLASS ELEMENTS ====
	# @subjectFullPath - full or relative path to subject or reference folder
	# @subjectFilenameList - list of all filenames in @subjectFullPath
	# @queryFullPath - full or relative path to query or input folder
	# @queryFilenameList - list of all filenames in @queryFullPath
	# @outputFullPath - path on system where output files will be written
	# @outputFilenameList - list of all filenames in @outputFullPath
	# @blatPath - full(preferable) or relative path to blat binary file
	# @blatParameterString - full string of blat optional parameter
	subjectFullPath = ''
	subjectFilenameList = []
	queryFullPath = ''
	queryFilenameList = []
	outputFullPath = ''
	outputFilenameList = []
	blatPath = ''
	blatParameterString = ''

	outputColumn = {}
	outputSeperator = {}
	outputColumn['blast9'] = ['query_id', 'subject_id', 'percent_identity', 'aligned_length', 'mismatches', 'gap_openings', 'q_start', 'q_end', 's_start', 's_end', 'e-value', 'bit_score']
	outputSeperator['blast9'] = '\t'

	# ==== CONSTRUCTOR ====
	# Initiate class with required parameters and load all filenames
	# ==== PARAMETERS ====
	# @subjectFullPath - full or relative path to subject or reference folder
	# @queryFullPath - full or relative path to query or input folder
	# @outputFullPath - path on system where output files will be written
	# @blatPath - full or relative path to blat binary file
	# @blatParameterString - ffull string of blat optional parameter
	def __init__(self, subjectFullPath, queryFullPath, outputFullPath, blatPath = '', blatParameterString = ''):
		# assigning class data
		self.subjectFullPath = subjectFullPath
		self.queryFullPath = queryFullPath
		self.outputFullPath = outputFullPath
		self.blatPath = blatPath
		self.blatParameterString = blatParameterString
		# check if paths ends with '/'
		# if not, add '/' to path data
		if not subjectFullPath[-1] == '/':
			self.subjectFullPath += '/'
		if not queryFullPath[-1] == '/':
			self.queryFullPath += '/'
		if not outputFullPath[-1] == '/':
			self.outputFullPath += '/'
		if not blatPath[-1] == '/':
			self.blatPath += '/'
		# load list of subject and query filenames into class
		self.loadFilenameList('subject')
		self.loadFilenameList('query')
		self.outputFilenameList = []


	# ==== loadFilenameList METHOD ====
	# Initiate class with required parameters and load all filenames
	def loadFilenameList(self, list_indicator = False):
		# ---- VARIABLE EXPLANATION ----
		# @f - temporary variable to keep each filename while checking
		# ------------------------------
		# printing progress to terminal
		if list_indicator == False or list_indicator == 'subject':
			print '|'
			print '| Loading file list from Subject input folders.'
			# list everything from @subjectFullPath and append to @ subjectFilenameList if it is a file
			self.subjectFilenameList = [ f for f in listdir(self.subjectFullPath) if isfile(join(self.subjectFullPath, f)) ]
		if list_indicator == False or list_indicator == 'query':
			print '|'
			print '| Loading file list from Query input folders.'
			# list everything from @queryFullPath and append to @ queryFilenameList if it is a file
			self.queryFilenameList = [ f for f in listdir(self.queryFullPath) if isfile(join(self.queryFullPath, f)) ]
		if list_indicator == False or list_indicator == 'output':
			print '|'
			print '| Loading file list from output folders.'
			# list everything from @outputFullPath and append to @ outputFilenameList if it is a file
			self.outputFilenameList = [ f for f in listdir(self.outputFullPath) if isfile(join(self.outputFullPath, f)) ]


	# ==== runBlat METHOD ====
	# run BLAT using according to class parameters
	def runBlat(self):
		# printing progress to terminal
		print '|'
		print '| Starting BLAT.'
		for subject_filename in self.subjectFilenameList:
			# ---- VARIABLE EXPLANATION ----
			# @subject_filename - a full name with extension from @subjectFilenameList
			# @subject_formatted_name - name of subject file without file extension
			subject_formatted_name = subject_filename[:subject_filename.find('.')]

			for query_filename in self.queryFilenameList:
				# ---- VARIABLE EXPLANATION ----
				# @query_filename - a full name with extension from @queryFilenameList
				# @query_formatted_name - name of query file without file extension
				# @command - command string to be execute
				# ------------------------------
				# printing progress to terminal
				print '| |'
				print '| | Running BLAT on Subject: %s and Query: %s' % (subject_filename, query_filename)

				query_formatted_name = query_filename[:query_filename.find('.')]

				command = '%sblat %s %s %s %s' % ( self.blatPath, \
												   self.subjectFullPath + subject_filename, \
												   self.queryFullPath + query_filename, \
												   self.outputFullPath + '%s--%s' % (subject_formatted_name, query_formatted_name), \
												   self.blatParameterString \
												 )
				system(command)
		# printing progress to terminal
		print '|'
		print '| BLAT operation completed.'


	# ==== inputTo2bit METHOD ====
	# convert reference and query fasta files to 2bit files
	def inputTo2bit(self):
		# printing progress to terminal
		print '|'
		print '| Converting FastA file to binary 2bit file.'
		print '|'
		print '| | Converting Subject sequences.'
		print '| |'
		if not exists(self.subjectFullPath + '2bit/'):
			system("mkdir " + self.subjectFullPath + '2bit/')

		for subject_filename in self.subjectFilenameList:
			# ---- VARIABLE EXPLANATION ----
			# @subject_filename - a full name with extension from @subjectFilenameList
			# @subject_filename_ext_index - index in @subject_filename string which the extension start
			# @subject_filename_2bit - new file name with .2bit extension
			# @command - command string to be execute
			subject_filename_ext_index = subject_filename.find('.') + 1
			subject_filename_2bit = subject_filename[:subject_filename_ext_index] + '2bit'

			command = '%sfaToTwoBit %s %s' % (self.blatPath, self.subjectFullPath + subject_filename, self.subjectFullPath + '2bit/' + subject_filename_2bit)
			system(command)
		# printing progress to terminal
		print '| | Changing Subject sequence file path.'
		# changing subject file path that will be used in BLAT
		self.subjectFullPath += '2bit/'

		# printing progress to terminal
		print '|'
		print '| | Converting Query sequences.'
		print '| |'
		if not exists(self.queryFullPath + '2bit/'):
			system("mkdir " + self.queryFullPath + '2bit/')

		for query_filename in self.queryFilenameList:
			# ---- VARIABLE EXPLANATION ----
			# @query_filename - a full name with extension from @queryFilenameList
			# @query_filename_ext_index - index in @query_filename string which the extension start
			# @query_filename_2bit - new file name with .2bit extension
			# @command - command string to be execute
			query_filename_ext_index = query_filename.find('.') + 1
			query_filename_2bit = query_filename[:query_filename_ext_index] + '2bit'

			command = '%sfaToTwoBit %s %s' % (self.blatPath, self.queryFullPath + query_filename, self.queryFullPath + '2bit/' + query_filename_2bit)
			system(command)
		# printing progress to terminal
		print '| | Changing Query sequence file path.'
		# changing query file path that will be used in BLAT
		self.queryFullPath += '2bit/'

		# reload subject and query file names into class
		self.loadFilenameList()


	# ==== parseOutput METHOD ====
	# 
	# ==== PARAMETERS ====
	# @blatOutputType
	def parseOutput(self, blatOutputType, chunk_start = '', chunk_end = ''):
		if len(self.outputFilenameList) == 0:
			self.loadFilenameList('output')
		blat_result_dict = {}

		if chunk_start == '' and chunk_end == '':
			output_filename_list = self.outputFilenameList
		else:
			output_filename_list = self.outputFilenameList[chunk_start:chunk_end]

		if blatOutputType == 'blast9':
			for an_output_filename in output_filename_list:
				blat_result_list = []
				with open(join(self.outputFullPath, an_output_filename), 'r') as an_output_file:
					for a_line in an_output_file:
						if not a_line.startswith('#') and len(a_line) > 5:
							result_dict = {}
							a_line = a_line.strip()
							a_line_list = a_line.split(self.outputSeperator['blast9'])
							for column_index, column_value in enumerate(a_line_list):
								result_dict[self.outputColumn['blast9'][column_index]] = column_value
							if int(result_dict['q_start']) <= int(result_dict['q_end']):
								result_dict['q_direction'] = '+'
							else:
								result_dict['q_direction'] = '-'

								tmp = result_dict['q_start']
								result_dict['q_start'] = result_dict['q_end']
								result_dict['q_end'] = tmp

							if int(result_dict['s_start']) < int(result_dict['s_end']):
								result_dict['s_direction'] = '+'
							else:
								result_dict['s_direction'] = '-'

								tmp = result_dict['s_start']
								result_dict['s_start'] = result_dict['s_end']
								result_dict['s_end'] = tmp
							
							blat_result_list.append(result_dict)
					# if an_output_filename == 'H37Rv--14448_6#54':
					# 	print blat_result_list

				blat_result_dict[an_output_filename] = blat_result_list

		return blat_result_dict


	# ==== filterOutputByLengthTypeColumns METHOD ====
	# 
	# ==== PARAMETERS ====
	# @moreOrLess - (STRING) canbe '>', '<', '>=', '<='
	def filterOutputByLengthTypeColumns(self, blatResultDict, filterColumn, filterValue, moreOrLess):
		filtered_blat_result_dict = {}
		for blat_result_key, result_list in blatResultDict.iteritems():
			if moreOrLess == '>':
				filtered_blat_result_list = [ r for r in result_list if int(r[filterColumn]) > filterValue]
			elif moreOrLess == '<':
				filtered_blat_result_list = [ r for r in result_list if int(r[filterColumn]) < filterValue]
			elif moreOrLess == '>=':
				filtered_blat_result_list = [ r for r in result_list if int(r[filterColumn]) >= filterValue]
			elif moreOrLess == '<=':
				filtered_blat_result_list = [ r for r in result_list if int(r[filterColumn]) <= filterValue]
			else:
				filtered_blat_result_list = result_list
			filtered_blat_result_dict[blat_result_key] = filtered_blat_result_list
		return filtered_blat_result_dict


	# ==== filterOutputByPosition METHOD ====
	# 
	# ==== PARAMETERS ====
	# @onQueryOrSubject - (STRING) can be either 'q' or 's'
	def filterOutputByPosition(self, blatResultDict, onQueryOrSubject, interestedStart, interestedEnd):
		filtered_blat_result_dict = {}

		for blat_result_key, result_list in blatResultDict.iteritems():
			if onQueryOrSubject in 'qs':
				filtered_blat_result_list = []
				for a_result in result_list:
					if not int(a_result[onQueryOrSubject + '_start']) < interestedStart and not int(a_result[onQueryOrSubject + '_end']) < interestedStart \
					and not int(a_result[onQueryOrSubject + '_start']) > interestedEnd and not int(a_result[onQueryOrSubject + '_end']) > interestedEnd:
						filtered_blat_result_list.append(a_result)
			else:
				filtered_blat_result_list = result_list
			filtered_blat_result_dict[blat_result_key] = filtered_blat_result_list
		return filtered_blat_result_dict

	def filterOutputSelect0evalue(self, blatResultDict):
		filtered_blat_result_dict = {}

		for blat_result_key, result_list in blatResultDict.iteritems():
			filtered_blat_result_list = []
			for a_result in result_list:
				if a_result['e-value'] == '0.0e+00':
					filtered_blat_result_list.append(a_result)
			filtered_blat_result_dict[blat_result_key] = filtered_blat_result_list
		return filtered_blat_result_dict

	def sortOutputByPosition(self, blatResultDict, onQueryOrSubject):
		sorted_blat_result_dict = {}

		for blat_result_key, result_list in blatResultDict.iteritems():
			sorted_result_list = sorted(result_list, key=lambda k: int(k[onQueryOrSubject + '_start']))

			sorted_blat_result_dict[blat_result_key] = sorted_result_list
		return sorted_blat_result_dict







if __name__ == '__main__':

	print '================================ blatGo ===================================='
	print 'To run BLAT through blatGo, please follow the instructions given after this.'
	print '============================================================================'
	print '                           SUBJECT SEQUENCES'
	print ' Please input path containing subject sequences, please input only the path'
	print ' and exclude the file name. Please make sure that:'
	print ' 1. The path you enter contain only fasta files you wish to use as subjects'
	print ' 2. Make sure that there\'s no typo and the path already exists.'
	print ' NOTE: full path is preferred here.'
	print '----------------------------------------------------------------------------'
	subjectFullPath = raw_input('SUBJECT FOLDER PATH: ')
	print '============================================================================'
	print '                            QUERY SEQUENCES'
	print ' Please input path containing query sequences, please input only the path'
	print ' and exclude the file name. Please make sure that: '
	print ' 1. The path you enter contain only fasta files you wish to use as query'
	print ' 2. Make sure that there\'s no typo and the path already exists.'
	print ' NOTE: full path is preferred here.'
	print '----------------------------------------------------------------------------'
	queryFullPath = raw_input('QUERY FOLDER PATH: ')
	print '============================================================================'
	print '                             OUTPUT PATH'
	print ' Please input path which the output files will be written.'
	print ' NOTE: full path is preferred here.'
	outputFullPath = raw_input('OUTPUT FOLDER PATH: ')
	print '============================================================================'
	print '                          BLAT BINARY PATH'
	print ' Please input path containing BLAT binary files, in case that you can run '
	print ' BLAT globally, you can leave this empty.'
	print ' NOTE: full path is preferred here.'
	blatPath = raw_input('BLAT BINARY PATH: ')
	print '============================================================================'
	print '                          BLAT PARAMETERS'
	print ' In case you wish to specify parameters for BLAT, please type in the full'
	print ' parameter string here.'
	blatParameterString = raw_input('BLAT PARAMETERS: ')
	print '============================================================================'
	print ' We will now start BLAT with you settings........'

	subjectFullPath = subjectFullPath.strip()
	queryFullPath = queryFullPath.strip()
	outputFullPath = outputFullPath.strip()
	if not exists(outputFullPath):
		system("mkdir " + outputFullPath)
	blatPath = blatPath.strip()
	blatParameterString = blatParameterString.strip()
	# blatParameterString = '-minIdentity=80 -minMatch=50 -out=blast9'

	blat = blatGo(subjectFullPath, queryFullPath, outputFullPath, blatPath, blatParameterString)
	blat.inputTo2bit()
	blat.runBlat()

	print ' blatGo has finished. Please see your output file in ' + outputFullPath
	print '============================== blatGo END =================================='